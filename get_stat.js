#!/usr/bin/env node

var fs = require('fs');
const path = require('path');
const readline = require('readline');
const input = readline.createInterface(process.stdin);

function getStatistics(url) {
  const pathUrl = path.parse(url);
  const file = path.join(pathUrl.dir, pathUrl.base);

  fs.readFile(file, 'utf-8', (err, data) => {
    if (err) {
      console.log('Ошибка: нет статистики об иргах или указан неверный путь');
      throw new Error(err);
    }
  
    // строку булевых значений из файла преобразуем в массив
    const statistics = JSON.parse(`[${data.slice(0, -1)}]`);
  
    winGames = statistics.filter(Boolean);
    lostGames = statistics.length - winGames.length;
    gamesStatisticsPer = (winGames.length * 100 / statistics.length).toFixed(1);
  
    console.log('Количество партий: ', statistics.length);
    console.log('Количество выигранных:', winGames.length);
    console.log('Количество проигранных партий:', lostGames);
    console.log('Процентное соотношение выигранных партий:', `${gamesStatisticsPer}%`);
    input.close();
  });
}

console.log('Укажите путь до файла со статистикой');
input.on('line', (url) => getStatistics(url));