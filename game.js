#!/usr/bin/env node

const fs = require('fs');
const path = require('path');
const readline = require('readline');
const input = readline.createInterface(process.stdin);

const number = Math.floor(Math.random() * 2) + 1; // Получение случайного целого числа в заданном интервале
let fileName = 'logs'; // дефолтное название для файла с логами
let result = false;

function startGame(str) {
  // проверяем, что в строке содержится 1 или 2
  const values = str.match(/[1||2]/) ? str.split(/[ ,]+/) : [];

  if (values && values.length) {
    values.forEach((value) => {
      if (Number(value) === number) {
        console.log('Вы выиграли');
        result = true;
      } else if (Number(value) && Number(value) !== number) {
        console.log('Вы проиграли');
      }
  
      if (!Number(value)) {
        fileName = value;
      }
    });

    // записываем результат
    const file = path.join(__dirname, `${fileName}.txt`);

    fs.appendFile(file, `${JSON.stringify(result)},`, err => {
      if (err) throw new Error(err)
    });

    input.close();
  } else {
    console.log('Введите: 1 (орел) или 2 (решка) и имя файла для логирования игры');
  }
};

console.log('Орел или решка: загадано 1 (орел) или 2 (решка) и имя файла для логирования игры');
input.on('line', (str) => startGame(str));


